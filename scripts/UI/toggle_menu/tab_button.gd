extends ToggleButton
class_name TabButton

export(NodePath) var target
export(String) var tooltip_text
